## The Big Book of Quotes from J.B. Wyatt and Other Clarion University Computer Science Faculty

This is the repository for [https://thebookofwyatt.com/](https://thebookofwyatt.com/)

A collection of quotes from several years at Clarion University

## Adding a Quote
Either [open an issue](https://gitlab.com/eeibevan/thebookofwyatt/issues/new)
or send an email to [quotes@thebookofwyatt.com](mailto:quotes@thebookofwyatt.com)
