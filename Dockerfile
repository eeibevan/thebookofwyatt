FROM nginx
COPY . /opt/thebookofwyatt
WORKDIR "/opt/thebookofwyatt"
RUN cp -R /opt/thebookofwyatt/* /usr/share/nginx/html
